<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Entity\OpeningTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class BookingController extends AbstractController
{
    /**
     * @Route("/booking", name="booking")
     */
    public function index(): Response
    {
        return $this->render('booking/index.html.twig', [
            'controller_name' => 'BookingController',
        ]);
    }

    /**
     * @Route("/booking/get-all-event-ajax", name="get_all_event_ajax", options={"expose"=true})
     */
    public function getAllEventAjax()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $allOpeningTimes = $entityManager->getRepository(OpeningTime::class)->findAll();

        $openingTimes = [];
        $bookings =  $this->getAllBookings();

        foreach ($allOpeningTimes as $openingTime) {
            $openingTimes[] = array(
                'start' => $openingTime->getStart()->format('Y-m-d'),
                'end' => $openingTime->getEnd()->format('Y-m-d'),
                'rendering' => 'background'
            );
        }

        $response = array_merge($openingTimes, $bookings);

        return new JsonResponse($response);
    }

    /**
     * @Route("/booking/get-opening-time-ajax", name="get_opening_time_ajax", options={"expose"=true})
     */
    public function getOpeningTimeAjax(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $openingTime = $entityManager->getRepository(OpeningTime::class)->findOneByDate($request->get('date'));
        $openingTimeStr = 'Sajnos zárva vagyunk.';

        if ($openingTime) {
            $openingTimeStr = $openingTime->getTime();
        }

        return new JsonResponse([
            'areWeOpen' => (bool) $openingTime,
            'msg' => $openingTimeStr
        ]);
    }

    /**
     * @Route("/booking/save-booking-ajax", name="save_booking_ajax", options={"expose"=true})
     */
    public function saveBookingAjax(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $date = $request->get('date');
        $start = $request->get('start');
        $end = $request->get('end');
        $name = $request->get('name');

        if (
            !$date ||
            !$start ||
            !$end ||
            !$name
        ) {
            return new JsonResponse([
                'error' => 'Az összes mező kitöltése kötelező'
            ]);
        }

        $dateIsInOpeningTime = $this->dateIsInOpeningTime($date, $start, $end);

        if (!$dateIsInOpeningTime['dateIsBetween']) {
            return new JsonResponse([
                'error' => $dateIsInOpeningTime['msg']
            ]);
        } else {
            $dateIsBooked = $this->dateIsBooked($date, $start, $end);

            if (!$dateIsBooked['dateIsBooked']) {
                $booking = new Booking();

                $booking->setStart(new \DateTime($date . ' ' . $start));
                $booking->setEnd(new \DateTime($date . ' ' . $end));
                $booking->setName($name);

                $entityManager->persist($booking);
                $entityManager->flush();
            } else {
                return new JsonResponse([
                    'error' => $dateIsBooked['msg']
                ]);
            }
        }

        return new JsonResponse([
            'success' => true
        ]);
    }

    private function dateIsBooked($date, $start, $end)
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();
            $booking = $entityManager->getRepository(Booking::class)->findOneByDate($date);

            $response['msg'] = 'A választott időpont sajnos már foglalt.';
            $response['dateIsBooked'] = false;

            if ($booking) {
                $bookingStart = $booking->getStart()->format('Y-m-d H:i');
                $bookingEnd = $booking->getEnd()->format('Y-m-d H:i');

                $checkedStartDate = $date . ' ' . $start;
                $checkedEndDate = $date . ' ' . $end;

                if (($checkedStartDate >= $bookingStart) && ($checkedStartDate <= $bookingEnd)) {
                    $response['dateIsBooked'] = true;

                    if (($checkedEndDate >= $bookingStart) && ($checkedEndDate <= $bookingEnd)) {
                        $response['dateIsBooked'] = true;
                    } else {
                        $response['dateIsBooked'] = false;
                    }
                }
            }

            return $response;
        } catch (\Exception $e) {
            return null;
        }
    }

    private function dateIsInOpeningTime($date, $start, $end)
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();
            $openingTime = $entityManager->getRepository(OpeningTime::class)->findOneByDate($date);

            if ($openingTime) {
                $openingTimeStart = $openingTime->getStart()->format('H:i');
                $openingTimeEnd = $openingTime->getEnd()->format('H:i');
                $openingTimeStr = $openingTimeStart . ' - ' .$openingTimeEnd;

                $openingStart = $openingTime->getStart()->format('Y-m-d H:i');
                $openingEnd = $openingTime->getEnd()->format('Y-m-d H:i');

                $checkedStartDate = $date . ' ' . $start;
                $checkedEndDate = $date . ' ' . $end;

                $response['msg'] = '';
                $response['dateIsBetween'] = false;

                if (($checkedStartDate >= $openingStart) && ($checkedStartDate <= $openingEnd)) {
                    $response['dateIsBetween'] = true;

                    if (($checkedEndDate >= $openingStart) && ($checkedEndDate <= $openingEnd)) {
                        $response['dateIsBetween'] = true;
                    } else {
                        $response['dateIsBetween'] = false;
                        $response['msg'] = 'A megadott időpontban sajnos már nem vagyunk nyitva. Nyitvatartás: ' . $openingTimeStr;
                    }
                } else {
                    $response['dateIsBetween'] = false;
                    $response['msg'] = 'A megadott időpontban sajnos nem vagyunk még nyitva. Nyitvatartás: ' . $openingTimeStr;
                }

                return $response;
            }
        } catch (\Exception $e) {
            return null;
        }
    }

    private function getAllBookings()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $allBookings = $entityManager->getRepository(Booking::class)->findAll();
        $bookings = [];

        foreach ($allBookings as $booking) {
            $bookings[] = array(
                'title' => $booking->getName(),
                'start' => $booking->getStart()->format('Y-m-d') . 'T' . $booking->getStart()->format('H:i:s'),
                'end' => $booking->getEnd()->format('Y-m-d') . 'T' . $booking->getEnd()->format('H:i:s'),
                'allDay' => false
            );
        }

        return $bookings;
    }
}
