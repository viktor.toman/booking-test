<?php

namespace App\DataFixtures;

use App\Entity\OpeningTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        // 2021-06-08 8-10 óra

        $openingTime1Start = new \DateTime();
        $openingTime1Start->setDate(2021, 6, 8);
        $openingTime1Start->setTime(8, 0);

        $openingTime1End = new \DateTime();
        $openingTime1End->setDate(2021, 6, 8);
        $openingTime1End->setTime(10, 0);

        $openingTime1 = new OpeningTime();
        $openingTime1->setDay('Tuesday');
        $openingTime1->setStart($openingTime1Start);
        $openingTime1->setEnd($openingTime1End);
        $openingTime1->setOpeningTimeType(OpeningTime::OPENING_TIME_TYPE_NO_REPEAT);
        $openingTime1->setTime('8-10');

        $manager->persist($openingTime1);
        $manager->flush();

        // 2021-01-01-től minden páros héten hétfőn 10-12 óra

        $start = new \DateTime('2021-01-01');
        $end = new \DateTime('2021-12-31');
        $interval = new \DateInterval('P1D');
        $period = new \DatePeriod($start, $interval, $end);

        foreach ($period as $date) {
            $openingTimeStart = $date;

            if ($date->format('D') == 'Fri') {
                $openingTimeStart->setTime(10, 0);

                $openingTimeEnd = clone $date;
                $openingTimeEnd->setTime(16, 0);

                $openingTime = new OpeningTime();
                $openingTime->setDay('Friday');
                $openingTime->setStart($openingTimeStart);
                $openingTime->setEnd($openingTimeEnd);
                $openingTime->setOpeningTimeType(OpeningTime::OPENING_TIME_TYPE_EVERY_WEEK);
                $openingTime->setTime('10-16');

                $manager->persist($openingTime);
                $manager->flush();
            }

            if (intval($date->format('W')) % 2 == 0) {
                if ($date->format('D') == 'Mon') {
                    $openingTimeStart->setTime(10, 0);

                    $openingTimeEnd = clone $date;
                    $openingTimeEnd->setTime(12, 0);

                    $openingTime = new OpeningTime();
                    $openingTime->setDay('Monday');
                    $openingTime->setStart($openingTimeStart);
                    $openingTime->setEnd($openingTimeEnd);
                    $openingTime->setOpeningTimeType(OpeningTime::OPENING_TIME_TYPE_EVEN_WEEK);
                    $openingTime->setTime('10-12');

                    $manager->persist($openingTime);
                    $manager->flush();
                }
            } else {
                if ($date->format('D') == 'Wed') {
                    $openingTimeStart->setTime(12, 0);

                    $openingTimeEnd = clone $date;
                    $openingTimeEnd->setTime(16, 0);

                    $openingTime = new OpeningTime();
                    $openingTime->setDay('Wednesday');
                    $openingTime->setStart($openingTimeStart);
                    $openingTime->setEnd($openingTimeEnd);
                    $openingTime->setOpeningTimeType(OpeningTime::OPENING_TIME_TYPE_ODD_WEEK);
                    $openingTime->setTime('12-16');

                    $manager->persist($openingTime);
                    $manager->flush();
                }
            }
        }

        // 2021-06-01-től 2021-11-30-ig minden héten csütörtökön 16-20 óra

        $start = new \DateTime('2021-06-01');
        $end = new \DateTime('2021-11-30');
        $interval = new \DateInterval('P1D');
        $period = new \DatePeriod($start, $interval, $end);

        foreach ($period as $date) {
            if ($date->format('D') == 'Thu') {
                $openingTimeStart = $date;
                $openingTimeStart->setTime(16, 0);

                $openingTimeEnd = clone $date;
                $openingTimeEnd->setTime(20, 0);

                $openingTime = new OpeningTime();
                $openingTime->setDay('Thursday');
                $openingTime->setStart($openingTimeStart);
                $openingTime->setEnd($openingTimeEnd);
                $openingTime->setOpeningTimeType(OpeningTime::OPENING_TIME_TYPE_EVERY_WEEK);
                $openingTime->setTime('16-20');

                $manager->persist($openingTime);
                $manager->flush();
            }
        }
    }
}
