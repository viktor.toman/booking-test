document.addEventListener('DOMContentLoaded', () => {
    let newBookingModal = new bootstrap.Modal(document.getElementById("newBookingModal"), {});
    let weAreClosedModal = new bootstrap.Modal(document.getElementById("weAreClosedModal"), {});
    let calendarEl = document.getElementById('calendar-holder');

    let calendar = new FullCalendar.Calendar(calendarEl, {
        locale: 'hu',
        defaultView: 'dayGridMonth',
        editable: false,
        selectable: false,
        weekNumbers: true,
        allDay: true,
        eventSources: [
            {
                id: 1,
                url: Routing.generate("get_all_event_ajax", {}),
                method: "POST",
                extraParams: {
                    filters: JSON.stringify({})
                },
                failure: () => {
                    toastr.error('There was an error while fetching FullCalendar!');
                },
            }
        ],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay',
        },
        plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
        timeZone: 'UTC+2',
        eventTimeFormat: {
            hour: '2-digit',
            minute: '2-digit',
            meridiem: false
        },
        loading: function (isLoading) {
            if (isLoading) {
                $('.loading').show();
            } else {
                $('.loading').hide();
            }
        },
        dateClick: function(info) {
            let dateStr = info.dateStr;
            $("#booking-form").trigger('reset');
            $('#date').val(dateStr);

            if (info.allDay) {
                $('.time-div').show();
            } else {
                $('.time-div').hide();
            }

            $.ajax({
                data: {
                    date: dateStr
                },
                type: "POST",
                dataType: "json",
                async: true,
                url: Routing.generate("get_opening_time_ajax", {}),
                success: function(data) {
                    if (data.areWeOpen) {
                        $('#opening-time-str').html(data.msg);
                        newBookingModal.show();
                    } else {
                        weAreClosedModal.show();
                    }
                },
                error: function(err) {
                    toastr.error("Hiba!.", "Sikertelen művelet.").css("width","500px");
                }
            });
        }
    });

    calendar.render();

    $("#booking-form").submit(function(e) {
        $('.loading').hide();
        e.preventDefault();
        let data = $("#booking-form").serializeArray();

        $.ajax({
            data: data,
            type: "POST",
            dataType: "json",
            async: true,
            url: Routing.generate("save_booking_ajax", {}),
            success: function(data) {
                if (data.error) {
                    toastr.error(data.error, "Sikertelen művelet.").css("width","500px");
                } else {
                    newBookingModal.hide();
                    calendar.refetchEvents();
                    toastr.success("Az időpont mentése sikeres volt.", "Sikeres művelet.").css("width","500px");
                }
            },
            error: function(err) {
                toastr.error("Az időpont mentése sikertelen volt.", "Sikertelen művelet.").css("width","500px");
            }
        });
    });

});