#INSTALLATION STEPS

1. git clone https://gitlab.com/viktor.toman/booking-test.git
2. composer install
3. Create the database
4. Setup .env file with database connect data
5. Run: php bin/console doctrine:migrations:migrate
6. Run: php bin/console doctrine:fixtures:load
7. Add this command in console: symfony server:start
8. Check in the browser: http://127.0.0.1:8000/booking